## API Shell

An empty API shell that can be used for anything!

It has functionality to load in settings from a file and start a basic API server with a /ping route.
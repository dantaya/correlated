package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

// Request structure
type Request struct {
	Key   string `json:"key"`
	Value string `json:"val"`
	TTL   int64  `json:"ttl"`
}

// Response type
type Response struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
	List    *Store      `json:"list,omitempty"`
}

// Item
type Item struct {
	Inserted time.Time
	TTL      int64
	Value    string
}

// Store
type Store map[string]Item

var store Store

// Removal
type Removal struct {
	Inserted time.Time
	TTL      int64
}

var list map[string]Removal

// Handler for the API server
type Handler struct{}

// ServeHTTP handles what to do with the request
func (h *Handler) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Set headers
	res.Header().Set("Content-Type", "application/json")

	// Handle options requests
	if req.Method == "OPTIONS" {
		// Set headers
		res.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, PATCH, DELETE")
		res.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization")

		res.Write([]byte(`{"status": "success"}`))
		return
	}

	// Get the request body
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(res, err.Error(), 500)
		return
	}

	// Unpack the body if it exists
	var request Request
	if len(body) > 0 {
		err := json.Unmarshal(body, &request)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	}

	// Define the response
	var response Response

	switch req.URL.Path {
	case "/ping":
		response.Status = "success"
		response.Message = "pong"
	case "/list":
		if req.Method != "GET" {
			http.Error(res, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}

		response.Status = "success"
		response.Message = "Here is whats in the db."
		response.List = &store
	case "/get":
		if req.Method != "GET" {
			http.Error(res, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}

		data, err := store.Get(request.Key)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		response.Status = "success"
		response.Message = "Heres your data for " + request.Key
		response.Data = &data
	case "/search":
		if req.Method != "POST" {
			http.Error(res, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}

		response.Status = "success"
		response.Message = "Successfully set " + request.Key
		response.Data = store.Search(request.Value)
	case "/set":
		if req.Method != "POST" {
			http.Error(res, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}

		err := store.Set(request.Key, request.Value, request.TTL)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		response.Status = "success"
		response.Message = "Successfully set " + request.Key
	case "/delete":
		if req.Method != "POST" {
			http.Error(res, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}

		err := store.Delete(request.Key)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}

		response.Status = "success"
		response.Message = "Successfully removed " + request.Key
	default:
		response.Status = "success"
		response.Message = "Hey, hows it going?"
	}

	// Pack the response
	responseBytes, _ := json.Marshal(response)

	// Return the response
	res.Write(responseBytes)
	return
}

func main() {

	// Loop until settings have loaded
	for !Settings.Load() {
		time.Sleep(5000 * time.Millisecond)
	}

	// Define the initial store
	store = make(map[string]Item)

	// Load the store
	store.Load()

	// Run the removal task
	go Remove()

	// Start the api
	if err := http.ListenAndServe(":"+Settings.Server.Port, new(Handler)); err != nil {
		fmt.Println("HTTP Api failed, reason: " + err.Error() + "\n")
	}
}

// Get a value from the store
func (s Store) Get(key string) (Item, error) {
	if _, ok := s[key]; !ok {
		return Item{}, errors.New("key doesnt exist")
	}

	return s[key], nil
}

// Search for the best match
func (s Store) Search(search string) string {
	var count int
	var best string
	searchLetters := strings.Split(search, "")

	for key, val := range s {
		// Exact match
		if val.Value == search {
			return key
		}

		// check sub string
		if strings.Contains(val.Value, search) {
			return key
		}

		// Loop over the letters of search string and see if they apear in the value in order
		currentCount := 0
		for _, val2 := range searchLetters {
			if strings.Contains(val.Value, val2) {
				currentCount++
			} else {
				break // letter doesnt exist in value so break out
			}
		}

		// current string is better than the last one
		if currentCount > count {
			count = currentCount
			best = key
		}
	}

	return best
}

// Set a value into the store
func (s Store) Set(key, value string, ttl int64) error {
	// @TODO: should prob do some checks to validate stuff here
	if key == "" || value == "" {
		return errors.New("missing some info")
	}

	item := Item{
		Inserted: time.Now(),
		TTL:      ttl,
		Value:    value,
	}

	s[key] = item

	s.Save()
	return nil
}

// Delete an entry from the store
func (s Store) Delete(key string) error {
	delete(s, key)

	s.Save()
	return nil
}

func (s *Store) Load() error {
	// Read the settings file
	bytes, err := ioutil.ReadFile("./store.db")
	if err != nil {
		fmt.Println("Failed to read store file: %v\n", err)
		return err
	}

	// Unpack the settings
	if err := json.Unmarshal(bytes, s); err != nil {
		fmt.Println("error unpacking the store file\n")
		return err
	}

	return nil
}

func (s Store) Save() error {

	// Pack the settings
	bytes, err := json.Marshal(s)
	if err != nil {
		fmt.Println("error packing the store file\n")
		return err
	}

	// Write to the file
	err = ioutil.WriteFile("./store.db", bytes, 0644)
	// f, err := os.Create("./store.db")
	// defer f.Close()
	if err != nil {
		fmt.Println("error writing the store file")
		return err
	}

	return nil
}

func Remove() {
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()
	done := make(chan bool)
	// go func() {
	// 	time.Sleep(10 * time.Second)
	// 	done <- true
	// }()
	for {
		select {
		case <-done:
			fmt.Println("Done!")
			return
		case <-ticker.C:
			for key, val := range store {
				// check the time between current time and inserted to see if its at or greater than TTL
				if val.TTL > 0 && int64(time.Since(val.Inserted).Seconds()) >= val.TTL {
					store.Delete(key)
					delete(list, key)
				}
			}
		}
	}
}
